import {questionAnswers, questionsNodeList, questionText} from "./modules/parser";
import {buildIndex} from "./modules/search";
var md5 = require("js-md5")

const index = buildIndex()
const pageData = questionsNodeList(document)
pageData.forEach( (question, qID) => {
    const qText = questionText(question);
    // console.log(qText)
    const qAnswers = questionAnswers(question);
    // console.log(qAnswers)

    try {
        const assumptions = index.search(qText.replace(":", ""))

        if (assumptions.length !== 0) {
            const relevant = assumptions.filter(item => {
                if (qAnswers.indexOf(item.ref) !== -1) {
                    return true
                } else {
                    return false
                }
            })

            let fullTextAnswer

            if (relevant.length !== 0) {
                relevant.sort(compareScores)
                fullTextAnswer = relevant[0].ref
            } else {
                assumptions.sort(compareScores)
                fullTextAnswer = assumptions[0]
            }

            qAnswers.forEach( (answer, aID) => {
                if (answer === fullTextAnswer) {
                    question.children[1].children[0].children[3].children[1].children[aID].children[1].setAttribute("style", "background: rgba(0, 128, 0, 0.3)")
                }
            })
        }
    } catch (e) {
        console.log("Error", e)
    }
})

function compareScores(a, b) {
    if (a.score > b.score) {
        return -1
    } else if (a.score < b.score) {
        return 1
    }
}

//document.getElementById("question-708939-1").children[1].children[0].children[3].children[1].children[0].children[1].setAttribute("style", "background: rgba(0, 128, 0, 0.3)")
// const pageData = Array.from(document.querySelectorAll("div.qtext"), el => el.innerText)

// it requires the connection of 18  wires and it's easy to install.
// it requires the connection of 18  wires and it's easy to install.