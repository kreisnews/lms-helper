// returns NodeList - all questions on current page
export function questionsNodeList(document) {
    return document.querySelectorAll("div.que")
}

// returns String - question text
export function questionText(q) {
    return q.querySelector("div.qtext").innerText
}

// returns Array of String with answers text
export function questionAnswers(q) {
    const divArray = Array.from(q.querySelector("div.answer").children)
    return divArray.map(el => {
        return el.children[1].innerText.substring(3)
    })
}