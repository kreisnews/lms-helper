var lunr = require("lunr")
// require("lunr-languages/lunr.stemmer.support")(lunr)
// require("lunr-languages/lunr.ru")(lunr)

const qa = {
    "data": [
        {
            "q": "Most advanced new automobiles have been equipped with a relaxing massage front seat.",
            "a": "False"
        },{
            "q": "in a polite voice the device will report on 14 different operation functions of the car.",
            "a": "True."
        },{
            "q": "The heart of the Voice Warning System is a microprocessor-based electronic speech module.",
            "a": "True."
        },{
            "q": "The driver can relax in the car listening to pleasant music.",
            "a": "Not stated."
        },{
            "q": "What have most advanced new cars been equipped with?",
            "a": "With a speaking instrument panel."
        },{
            "q": "How many operation functions can the device report the driver on?",
            "a": "14 functions"
        },{
            "q": "What can the driver even program The Voice Warning System to do?",
            "a": "to announce the time"
        },{
            "q": "What kind of voice will give a report to a driver?",
            "a": "a polite female voice"
        },{
            "q": "The car will keep the steady speed if the driver programs it.",
            "a": "Not stated."
        },{
            "q": "The driver can even program the Voice Warning Sysytem to announce the time or to give a low-fuel warning.",
            "a": "True."
        },{
            "q": "What does the  device installation of the Voice Warning System require ?",
            "a": "it requires the connection of 18  wires and it's easy to install."
        },{
            "q": "Advanced new automobiles have been equipped with speaking instrument panel providing the driver with safety warnings from special electronic circuits.",
            "a": "True."
        },{
            "q": "What warnings can the speaking panel provide",
            "a": "safety warnings"
        },{
            "q": "What is the heart of the Voice Warning System?",
            "a": "a microprocessor-based electronic speech module"
        },{
            "q": "Advanced new automobiles have been equipped with speaking instrument panel providing the driver with safety  warnings from special electronic circuits.",
            "a": "True."
        },{
            "q": "Most advanced cars  have been equipped with powerful headlights providing excellent vision. ",
            "a": "Not stated."
        }
    ]
}

// returns lunr index with loaded questions and answers
export function buildIndex() {
    return lunr(function() {
        // this.use(lunr.ru)
        this.ref('a')
        this.field('q')

        qa.data.forEach((el) => {
            this.add(el)
        })
    })
}